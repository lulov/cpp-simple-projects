#include <iostream>

float fazerCalculo(float valor)
{
    return valor;
}

float pegarValor(float num_input)
{
    std::cout << "Me informe o número: ";
    std::cin >> num_input;
    return num_input;
}

void mostrarValor(float outputFinal)
{
    std::cout.precision(4);
    std::cout << "O resultado é: " << outputFinal << "\n";
}

char qualOperacao(char operacao_escolhida)
{
    std::cout << "Me informe o tipo de operação: ";
    std::cin >> operacao_escolhida;
    std::cout << "\n";
    return operacao_escolhida;
}

int main()
{
    char estiloCalculo { qualOperacao(estiloCalculo) };

    while ( ( estiloCalculo != '+') &&
         ( estiloCalculo != '-' ) &&
         ( estiloCalculo != '/' ) &&
         ( estiloCalculo != '*') )
    {
        std::cout << "Algo deu errado! Tente novamente!" << "\n";
        estiloCalculo = qualOperacao(estiloCalculo);
    }

        float x { pegarValor(x) };
        float y { pegarValor(y) };        

        if ( estiloCalculo == '+')
        {
            float somar = { fazerCalculo(x + y) };
            mostrarValor(somar);
        }

        else if ( estiloCalculo == '-')
        {
            float diminuir = { fazerCalculo(x - y) };
            mostrarValor(diminuir);
        }

        else if ( estiloCalculo == '*')
        {
            float multiplicar = { fazerCalculo(x * y) };
            mostrarValor(multiplicar);
        }

        else if ( estiloCalculo == '/')
        {
            float dividir = { fazerCalculo(x / y) };
            mostrarValor(dividir);
        } 
       
    return 0;
}



